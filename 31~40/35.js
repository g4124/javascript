// 이 값 중 어느 것이 거짓 같은 값일까요?
0;
new Number(0);
('');
(' ');
new Boolean(false);
undefined;

// A: 0, '', undefined
// B: 0, new Number(0), '', new Boolean(false), undefined
// C: 0, '', new Boolean(false), undefined
// D: 모든 값은 거짓


// A.

// false값 아래 참고
// undefined
// null
// NaN
// false
// '' (빈 문자열)
// 0
// -0
// -0n (BigInt(0))
// new Number 그리고 new Boolean과 같은 생성자 함수는 참 같은 값이에요.