// 무엇이 출력 될까요?
console.log([
  [0, 1],
  [2, 3],
].reduce(
  (acc, cur) => {
    return acc.concat(cur);
  },
  [1, 2]
));

// C
// reduce 시작 초기값 [1,2]
// 그 뒤 [[0,1], [2,3]] 배열을 순차적으로 contcat