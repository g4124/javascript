//무엇이 출력 될까요?
const numbers = [1, 2, 3];
numbers[10] = 11;
console.log(numbers);

// A: [1, 2, 3, 7 x null, 11]
// B: [1, 2, 3, 11]
// C: [1, 2, 3, 7 x empty, 11]
// D: SyntaxError

// C