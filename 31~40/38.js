(() => {
    let x, y;
    try {
      throw new Error();
    } catch (x) {
      (x = 1), (y = 2);
      console.log(x);
    }
    console.log(x);
    console.log(y);
})();

// A: 1 undefined 2
// B: undefined undefined undefined
// C: 1 1 2
// D: 1 undefined undefined

// A.
// x,y 선언 후 try block 안에 고의로 Error 발생시켜
// catch block이동 후 catch의 배개변수 x=1(Error => x 치환한것), y=2로 초기화
// x출력(이때 x는 Error)
// x출력, y출력