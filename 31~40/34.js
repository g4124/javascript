// 무엇이 출력 될까요?
function sayHi() {
  return (() => 0)();
}

// A: "object"
// B: "number"
// C: "function"
// D: "undefined"

console.log(sayHi()); // 0
console.log(typeof sayHi()); // number

// B.
// return의 즉시실행함수는 0을 반환하기 때문