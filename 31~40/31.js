// 버튼을 클릭했을때 event.target은 무엇일까요?
<div onclick="console.log('first div')">
  <div onclick="console.log('second div')">
    <button onclick="console.log('button')">Click!</button>
  </div>
</div>

// A: 외부의 div
// B: 내부의 div
// C: button
// D: 중첩된 모든 요소의 배열

// C.