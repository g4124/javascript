// p태그를 클릭하면 출력된 로그는 무엇일까요?
<div onclick="console.log('div')">
  <p onclick="console.log('p')">Click here!</p>
</div>

// A: p div
// B: div p
// C: p
// D: div

// A.