const person = { name: 'Lydia' };

function sayHi(age) {
  console.log(`${this.name} is ${age}`);
}

sayHi.call(person, 21);
sayHi.bind(person, 21);

// A: undefined is 21 Lydia is 21
// B: function function
// C: Lydia is 21 Lydia is 21
// D: Lydia is 21 function

// call, bind 모두 참조하는 객체 반환가능.
// call : 첫번째 인자에 this로 세팅하고 싶은 객체를 넘겨준 this변경 후 즉시실행
// bind : 즉시 실행하지 않고 복사본을 반환


// 참고
var example = function (a, b, c) {
    return a + b + c;
  };
example(1, 2, 3);
example.call(null, 1, 2, 3);
example.apply(null, [1, 2, 3]);

// 이때 null이 하는 역할 => this의 대체


var obj = {
    string: 'zero',
    yell: function() {
      alert(this.string);
    }
};

var obj2 = {
    string: 'what?'
};

obj.yell(); // 'zero';
obj.yell.call(obj2); // 'what?'

// 참고 zerocho blog
// https://www.zerocho.com/category/JavaScript/post/57433645a48729787807c3fd


// 응용
function example() {
    console.log(arguments);
}

example(1, 'string', true); // argument : [1, 'string', true]


// 형태는 배열이지만 정식배열이 아닌 형태만 따온 무언가, 따라서 join 함수가 없음
function example1() {
    console.log(arguments.join());
}

example1(); // Uncaught TypeError: arguments.join is not a function


// 배열의 함수를 빌려 유사배열을 문자열로 join한것
function example1() {
    console.log(Array.prototype.join.call(arguments));
}

example1(); // '1,string,true'