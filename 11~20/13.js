// 13. 이벤트 전달의 3단계는 무엇일까요?
// A: Target > Capturing > Bubbling
// B: Bubbling > Target > Capturing
// C: Target > Bubbling > Capturing
// D: Capturing > Target > Bubbling

// D
// capturing 단계 동안에, 이벤트는 조상 요소를 거쳐 target 요소까지 내려가요. 
// 그런 다음 target 요소에 도달하고, bubbling이 시작돼요.