function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  const member = new Person('Lydia', 'Hallie');
  Person.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
  };
  
  console.log(member.getFullName());
  // TypeError
  // static 함수처럼 호출해야함, 객체에 바인딩하려면 프로토 타입 내부로