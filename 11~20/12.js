function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    console.log(this);
}
  
const lydia = new Person('Lydia', 'Hallie');
const sarah = Person('Sarah', 'Smith');

console.log(lydia);
console.log(sarah);

// Person 객체 출력
// undefined 출력
// 객체 생성과 함수 return의 차이, new 생성자를 이용한경우 this는 새로운 객체를 참조.
// new를 추가하지 않으면 전역변수를 참조함