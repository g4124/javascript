function checkAge(data) {
    if (data === { age: 18 }) {
        console.log('You are an adult!');
    } else if (data == { age: 18 }) {
        console.log('You are still an adult.');
    } else {
        console.log(`Hmm.. You don't have an age I guess`);
    }
}
  
checkAge({ age: 18 });

function human(age) {
    this.age = age;
}

function checkAge(data) {
    const test = new human(18);

    console.log(typeof data);
    console.log(typeof test);
    console.log(data === test);
    console.log(data == test);
    
    if (data === test) {
        console.log('You are an adult!');
    } else if (data == test) {
        console.log('You are still an adult.');
    } else {
        console.log(`Hmm.. You don't have an age I guess`);
    }
}
  
checkAge(new human(18));

  // === : 값과 타입비교
  // == : 자동으로 형변환한 뒤 값만 비교
  // 같은 human 객체더라도 메모리주소를 참조하므로 falase, 원시형만 비교하자