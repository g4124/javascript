function getPersonInfo(one, two, three) {
    console.log(one);
    console.log(two);
    console.log(three);
}
  
const person = 'Lydia';
const age = 21;
  
getPersonInfo`${person} is ${age} years old`;

// 태그가 지정된 템플릿 리터럴 사용시 첫 인수의 값은 ` is years old` 문자열값의 배열
// 기준은 공백을 기준으로 스플릿된것으로보임. 나머지 2,3번째 매개변수는 태그가 순서대로 들어감