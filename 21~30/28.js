// 무엇이 출력 될까요?

String.prototype.giveLydiaPizza = () => {
    return 'Just give Lydia pizza already!';
};
  
const test_name = 'Lydia';

test_name.giveLydiaPizza();


// A: "Just give Lydia pizza already!"
// B: TypeError: not a function
// C: SyntaxError
// D: undefined

// A
// 왜 출력함수가 없는데 출력을 원할까