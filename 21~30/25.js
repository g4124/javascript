// 무엇이 출력 될까요?
const obj = { a: 'one', b: 'two', a: 'three' };
console.log(obj);

// A: { a: "one", b: "two" }
// B: { b: "two", a: "three" }
// C: { a: "three", b: "two" }
// D: SyntaxError

// C
// a = 'one' 저장 후 a = 'three' 최신값을 유지하기 때문