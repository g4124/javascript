const obj = { 1: 'a', 2: 'b', 3: 'c' };
const set = new Set([1, 2, 3, 4, 5]);

obj.hasOwnProperty('1');
obj.hasOwnProperty(1);
set.has('1');
set.has(1);

// true, true, false, true
// 키값은 문자열로 지정 및 입력하지 않아도 기본적으로 문자열
// set의 has는 해당 값이 있는지 조회하는것이기 때문