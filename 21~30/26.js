// JavaScript의 전역 실행 컨텍스트는 두가지를 만들어요: 전역객체와 "this" 키워드에요.

// A: true
// B: false
// C: 경우에 따라 달라요

// A.
// 전역객체(js => window, node => global), this