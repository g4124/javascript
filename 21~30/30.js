// 무엇이 출력 될까요?
const foo = () => console.log('First');
const bar = () => setTimeout(() => console.log('Second'));
const baz = () => console.log('Third');

bar();
foo();
baz();

// A: First Second Third
// B: First Third Second
// C: Second First Third
// D: Second Third First

// B.
// javascript의 이벤트 루프 or 답 참고