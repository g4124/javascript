// 무엇이 출력 될까요?

const a = {};
const b = { key: 'b' };
const c = { key: 'c' };

a[b] = 123;
a[c] = 456;

console.log(a[b]);

// A: 123
// B: 456
// C: undefined
// D: ReferenceError

// B
// 객체 키는 자동으로 문자열 반환
// 객체를 문자열화하면 object Object(내부 볼 수 없음)
// a['object Object'] = 123;
// a['object Object'] = 456;