const sum = eval('10*10+5');

// 105 출력
/**
 * eval은 매개변수로 들어간 문자열의 계산식이나 함수를 실행함
 * 하지만 사용을 권장하지 않다.
 * 1. caller 권한으로 실행
 *  1-1. 전역 범위가 아닌 브라우저 전체 범위이기 때문에 분별없이 정보접근이 가능해짐(보안 취약)
 *  1-2. eval대신 Function 이용
 *   - eval 전역범위 밖까지 변수명 탐색
 *   - Function 전역범위 한정
 *  -> 속도차이 명확
 * 
 * 2. flutter in app webview에서는 eval
 *  2-1. 코드를 iframe으로 실행하여 결과를 가져오고 반환하기 때문.
 *  2-2. 내부에 evaluateJavascript -> "window.`String Value'" 조합하여 postMessage 형태로 호출
 *  2-3. 하지만 Function을 안쓰고 eval을 쓰는지 모르겠음
 * 
 * 참고
 * // flutter in app webview evaluateJavascript
 * https://github.com/pichillilorenzo/flutter_inappwebview/blob/master/lib/src/in_app_webview/in_app_webview_controller.dart
 * https://github.com/pichillilorenzo/flutter_inappwebview/blob/master/lib/src/web_message/web_message_listener.dart
 * https://github.com/pichillilorenzo/flutter_inappwebview/blob/f06bcdf695847a0187a31ab607b2bfe236fb0057/android/src/main/java/com/pichillilorenzo/flutter_inappwebview/in_app_webview/InAppWebView.java
 * 
 * // flutter evaluateJavascript 예제
 * https://medium.com/flutter-community/flutter-webview-javascript-communication-inappwebview-5-403088610949
 * // parent, child frame 통신방법
 * https://developer.mozilla.org/ko/docs/Web/API/Window/postMessage
 */
