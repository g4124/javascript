let c = { greeting: 'Hey!' };
let d;

d = c;
c.greeting = 'Hello';
console.log(d.greeting);

// 얕은복사 되었음
// 따로 값을 할당하려면 깊은 복사를 할 것