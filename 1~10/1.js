function sayHi() {
    console.log(name);
    console.log(age);
    var name = 'Lydia';
    let age = 21;
  }
  
  sayHi();

  /**
   * 'undefined'
   * 'Reference Err'
   * var = 전역변수, 런타임 이전에 할당
   * var는 런타임 이전에서 할당되어 초기값인 undefined 출력됨
   * let은 런타임에 선언, 범위형
   * name은 선언화 초기화가 런타임 전에 이루어져 호이스팅이 가능함
   * age는 선언전에 호출되어 Err
   */