for (var i = 0; i < 3; i++) {
    setTimeout(() => console.log(i), 1);
}
  
for (let i = 0; i < 3; i++) {
    setTimeout(() => console.log(i), 1);
}

  /**
   * setTimeout은 백그라운드에서 타이머가 돌아간 뒤 다시 작업큐로 넣기때문에
   * var Loop는 참조하고있는 값의 주소는 동일하지만
   * let Loop는 범위변수로 참조하는 값이 달라지기 때문
   */