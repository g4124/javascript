function test() {
    this.isTestScope = true;

    const shape = {
        radius: 10,
        diameter() {
          console.log(this.radius * 2);
        },
        perimeter1: () => console.log(this),
        perimeter2: function() {
            console.log(this);
        }
    };
    shape.diameter();
    shape.perimeter1();
    shape.perimeter2();
}

test();
// 20
// 전역객체
// shape객체


// -------------------------------------------------------------


const shape = {
    radius: 10,
    diameter() {
      console.log(this.radius * 2);
    },
    perimeter1: () => console.log(this),
    perimeter2: function() {
        console.log(this);
    }
};
  
shape.diameter(); // 20
shape.perimeter1(); // {}
shape.perimeter2(); // shape 객체

/**
 * 1. 일반함수 this === 호출자
 * 2. 화살표함수 this === 현재 주변 함수 스코프 참조
 * (참고: https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Functions/Arrow_functions)
 */