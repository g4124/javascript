let a = 3;
let b = new Number(3);
let c = 3;

console.log(a == b);
console.log(a === b);
console.log(b === c);

// == : 값비교, === : 값, 타입 모두 비교
// 사실 Number는 int가 아닌 Number라는 하나의 객체