console.log(+true);
console.log(+false);
console.log(!'Lydia');
console.log(!'');
console.log(+undefined);
console.log(+null);
console.log(+NaN);

// true 기본값 = 1, false 기본값 = 2
// 문자열이 존재할때 true, 빈값일때 false
// +undefined = NaN
// null 기본값 0
// NaN은 NaN