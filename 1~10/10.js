function bark() {
    console.log('Woof!');
}
  
bark.animal = 'dog';
console.log(bark); //  [Function: bark] { animal: 'dog' }

// bark 함수 내부에 animal = dog 추가


function bark2() {
    this.isTest = true;
    console.log('Woof!');
}

bark2.animal = 'cat';

const temp = new bark2();
console.log(bark2); // [Function: bark2] { animal: 'cat' }
console.log(temp); // bark2 { isTest: true }

temp.animal = 'Ryan';
console.log(temp); // bark2 { isTest: true, animal: 'Ryan' }